import React, { Component } from 'react';
import Modal from '../Modal/Modal';

class Button extends Component {
    constructor(props){
        super(props);

    }

    state = {
        isModalOpen: false
    };
      
    toggleModal() {
        this.setState({isModalOpen: !this.state.isModalOpen})
      }

      openModal() {
        this.setState({isModalOpen: true})
        console.log("open")
      }

      closeModal() {
        this.setState({isModalOpen: false})
        console.log("close")
      }


    render() {
        const {isModalOpen} = this.state;

        return (
            <div>
                <button style={this.props.backgroundColor}
                className = "add_to_cart_button"
                onClick = {this.openModal.bind(this)}
                id={this.props.id}
                >{this.props.buttonText}</button>

                {isModalOpen ?  
                    <Modal 
                    modalColor = {this.props.modalBackground}
                    headerText = {this.props.header}
                    modalText = {this.props.mainText}
                    closeBtn = {this.props.closeBtn}
                    modalOpen = {this.openModal.bind(this)}
                    modalClose = {this.closeModal.bind(this)}
                    actions = {this.props.actions}
                    productName = {this.props.productName}
                    productArticul = {this.props.productArticul}
                    
                /> : null}
                
            </div>
        );
    }
}

export default Button;