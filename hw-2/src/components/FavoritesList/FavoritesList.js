import React, { Component } from 'react';

class FavoritesList extends Component {
    constructor(props){
        super(props)
        this.state = {
            styleState : {
                backgroundColor: "#fff",
                color: "",
            },
            
        }
    }
    
    addToLocalStorage = () => {
        const {productName, productArticul} = this.props
        const isFavorite = localStorage.getItem(productArticul) !== null;
        if(isFavorite){
            localStorage.removeItem(productArticul)
            console.log(productArticul)
            this.setState({
                styleState : { 
                color: "#000",
            }
            });
        } else {
            localStorage.setItem(productArticul, productName + ' in favorite')
            this.setState({
                styleState : { 
                color: "#e12120",
            }
            });
        }

    };
    
    componentDidMount() {
        const { productArticul} = this.props
        const Favorite = localStorage.getItem(productArticul) !== null;
        if(Favorite){
            this.setState({
                styleState : { 
                color: "#e12120",
            }
            });
        }
      }

    render() {
        console.log(this.state.styleState)
        
        return (
            <>
                <button 
                className = "fav-ico" 
                onClick={this.addToLocalStorage.bind(this)}
                style ={this.state.styleState}
                ><i 
                class="far fa-star"
                ></i></button>
            </>
        );
    }
}

export default FavoritesList;