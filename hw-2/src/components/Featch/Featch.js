import React, { Component } from 'react';
import ProductList from '../ProdactList/ProductList';

class Featch extends Component {
    constructor(props) {
        super(props);
        this.state = {
          error: null,
          isLoaded: false,
          items: []
        };
    }
    
    componentDidMount() {
        fetch("http://localhost:3004/items/")  
        .then(res => res.json())
        .then(
            (res) => {
                this.setState({
                    isLoaded: true,
                    items: res,
                });
                console.log(res)
            }
        )
    }
    
    render() {
        return (
            <ProductList
            listState = {this.state}/>
        );
    }
}

export default Featch;