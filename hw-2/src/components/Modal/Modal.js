import React, { Component } from 'react';


class Modal extends Component {
    constructor(props){
        super(props)
    }
    
         
        
    

    prevDef (e) {
        e.preventDefault()
        e.stopPropagation()
    }

    addToLocalStorage = () => {
        const {productName, productArticul} = this.props
        localStorage.setItem(`Cart ${productArticul}`, productName + ' in shopping cart')
      };

      

    render() {
        
        return (
            <div>
                <div className="modal-overlay" onClick = {this.props.modalClose}>

                    <div className="modal-window"
                    style={this.props.modalColor}
                    onClick = {this.prevDef}>

                        <div className = "modalHeader">
                            <h3>{this.props.headerText}</h3>
                            {this.props.closeBtn ?
                            <p onClick = {this.props.modalClose} className = "x-close">X</p> 
                            : null} 
                        </div>

                        <p>{this.props.modalText}</p>

                        <div className = "actionsBtnConteiner" >
                            <button className = "actionsBtn" onClick={this.addToLocalStorage.bind(this)}>Confirm</button>
                            <button className = "actionsBtn" onClick = {this.props.modalClose}>Cancel</button>
                        </div>
                         

                    </div>

                </div>
            </div>
        );
    }
}

export default Modal;