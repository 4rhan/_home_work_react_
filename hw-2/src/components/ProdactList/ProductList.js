import React, { Component } from 'react';
import ProductItem from '../ProductItem/ProductItem';

class ProductList extends Component {
    constructor(props) {
        super(props);
        
    }
    render() {
        const {listState} = this.props;
        return (
            <div>
                <div className = "product_list">
                    {listState.items.map(item => (
                        <ProductItem item = {item}/>
                        
                    ))}
                </div>
            </div>
        );
    }
}

export default ProductList;