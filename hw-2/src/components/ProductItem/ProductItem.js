import React, { Component } from 'react';
import Button from '../Button/Button';
import FavoritesList from '../FavoritesList/FavoritesList';

class ProductItem extends Component {
    render() {
        const {item} = this.props;
        
        return (
            <>
                <div className = "product_item" key={item.articul}>
                    <div className = "product_Img" style = {{ backgroundImage: `url(${item.imgUrl})`}}></div>
                    <div className = "product_price_and_favIco">
                        <h3>{item.name}</h3>
                        <FavoritesList
                            productName = {item.name}
                            productArticul = {item.articul}
                        />
                        <p>{item.price}</p>
                    </div>
                    <div>
                      <Button 
                        buttonText = "Add to cart"
                        header = {"Add to cart?"}
                        mainText = {"Сonfirm adding to cart"}
                        closeBtn = {true}
                        productName = {item.name}
                        productArticul = {item.articul}
                        modalBackground = {{backgroundColor: "blanchedalmond",}}
                        actions = {<><button className = "actionsBtn">Confirm</button>
                                  <button className = "actionsBtn">Cancel</button></>}
                        />
                      </div>
                </div> 
                
            </>
        );
    }
}

export default ProductItem;