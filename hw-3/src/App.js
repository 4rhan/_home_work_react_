import './App.css';
import Button from './components/Button/Button';
import Header from './components/Header/Header';
import ProductList from './components/ProdactList/ProductList';
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import FavoritesPage from './components/FavoritesPage/FavoritesPage';
import CartPage from './components/CartPage/CartPage';

function App(){


  
    return (
      <div className = "conteiner">
        <Header>
        </Header>
        <Switch>
        <Route exact path="/productlist">
          <ProductList/>
        </Route>
          <Route exact path="/favoritespage">
          <FavoritesPage/>
        </Route>
          <Route exact path="/cartpage">
          <CartPage/>
        </Route>
      </Switch>
      
      </div>
    );
  
}

export default App;