import React, {useState, useEffect} from 'react';
import Modal from '../Modal/Modal';

function Button(props) {
    const {item} = props;

    const [modalActive, setModalActive] = useState(false)
    const [buttonText, setButtonText] = useState({text: ""})
    const [buttonColor, setButtonColor] = useState({backgroundColor: "#"})
    const [modalQuestion, setModalQuestion] = useState({text: "#"})

    useEffect(() => {
        if(item.atCart){
            setButtonText({text: "Remove from the cart"})
            setButtonColor({backgroundColor: "#8B0000"})
            setModalQuestion({text: "Are you sure you want to remove an item from your cart?"})
        } else {
            setButtonText({text: "Add to cart"})
            setButtonColor({backgroundColor: "#c0c0c0"})
            setModalQuestion({text: "Add to cart?"})
        }
    },[])

    function addOrDelFromCart() {
        if(item.atCart){
            (fetch(`http://localhost:3004/items/${item.id}`,
            {
                method: "PATCH",
                body: JSON.stringify({
                    "atCart": false,
                }),
                headers: { 'Content-Type': 'application/json' }
            }))
            setButtonText({text: "Add to cart"})
            setButtonColor({backgroundColor: "#c0c0c0"})
            setModalQuestion({text: "Add to cart?"})
            setModalActive(false)
        } else {
            (fetch(`http://localhost:3004/items/${item.id}`,
            {
                method: "PATCH",
                body: JSON.stringify({
                    "atCart": true,
                }),
                headers: { 'Content-Type': 'application/json' }
            }))
            setButtonText({text: "Remove from the cart"})
            setButtonColor({backgroundColor: "#8B0000"})
            setModalQuestion({text: "Are you sure you want to remove an item from your cart?"})
            setModalActive(false)
        }
    }

    return (
        <div>
            <button
            className = "add_to_cart_button"
            onClick = {() => setModalActive(true)}
            style={buttonColor}
            >{buttonText.text}</button>

                <Modal active={modalActive} setActive={setModalActive}>
                    <div className = "modalHeader">
                        <h3>{modalQuestion.text}</h3>
                        <p onClick = {() => setModalActive(false)} className = "x-close">X</p>
                    </div>
                    <div className = "actionsBtnConteiner" >
                        <button className = "actionsBtn" onClick={addOrDelFromCart}>Confirm</button>
                        <button className = "actionsBtn" onClick = {() => setModalActive(false)}>Cancel</button>
                    </div>
            </Modal>
            
        </div>
    );
}

export default Button;