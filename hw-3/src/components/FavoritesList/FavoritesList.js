import React, { useState, useEffect } from 'react';

function FavoritesList(props) {

    const [favIconStule, setFavIconStule] = useState({color: "#c0c0c0",})
    
    const {item} = props

    useEffect(() => {
        if(item.atFavorites){
            setFavIconStule({color: "#8B0000",});
        } else {
            setFavIconStule({color: "#c0c0c0",});
        }
    },[])

    function addToLocalStorage() {
        if(item.atFavorites){
            (fetch(`http://localhost:3004/items/${item.id}`,
            {
                method: "PATCH",
                body: JSON.stringify({
                    "atFavorites": false,
                }),
                headers: { 'Content-Type': 'application/json' }
            }))
            setFavIconStule({color: "#c0c0c0",});
        } else {
            (fetch(`http://localhost:3004/items/${item.id}`,
            {
                method: "PATCH",
                body: JSON.stringify({
                    "atFavorites": true,
                }),
                headers: { 'Content-Type': 'application/json' }
            })) 
            .then(response => console.log(response))
            setFavIconStule({color: "#8B0000",});
        }
    };
        
    return (
            <button 
            className = "star-icon" 
            onClick={addToLocalStorage}
            style ={favIconStule}
            ><i className="far fa-star"></i></button>
        
    );
}


export default FavoritesList;