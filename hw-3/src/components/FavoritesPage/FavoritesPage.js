import React, { useState, useEffect  } from 'react';
import ProductItem from '../ProductItem/ProductItem';

function FavoritesPage() {
    
        const [itemArr, setItemArr] = useState({
            error: null,
            isLoaded: false,
            items: []
          })
        
        useEffect(() => {
                fetch("http://localhost:3004/items/")
            .then(res => res.json())
            .then(
                (res) => {
                    setItemArr({
                        isLoaded: true,
                        items: res,
                    });
                }
                )
            }, []);
            
            
            itemArr.items.forEach(function(item, index, array){
                console.log(array)
                if(!item.atFavorites){
                    delete array[index]
                }
            })

        return (
            <div>
                <div className = "product_list">
                    {itemArr.items.map(item => (
                        <ProductItem item = {item}/>
                    ))}
                </div>
            </div>
        );
    
    }

export default FavoritesPage;