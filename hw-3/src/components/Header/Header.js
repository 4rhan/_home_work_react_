import React from 'react';
import CartPage from '../CartPage/CartPage';
import FavoritesPage from '../FavoritesPage/FavoritesPage';
import {BrowserRouter as Router, Switch, Route, NavLink} from "react-router-dom";
import Button from '../Button/Button';


function Header(props) {
    return (
        <header>
            <nav>
              <ul>
                <li>
                  <NavLink to="/productlist">Product List</NavLink>
                </li>
                <li>
                  <NavLink to="/favoritespage">Your favorites</NavLink>
                </li>
                <li>
                  <NavLink to="/cartpage">Your cart</NavLink>
                </li>
              </ul>
            </nav>

        </header>
    );
}

export default Header;