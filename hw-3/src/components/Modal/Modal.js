import React from 'react';


function Modal(props) {

    const {
        active,
        setActive,
        children} = props;

    return (
        <div>
            <div className={active ? "modal-overlay active" : "modal-overlay"} onClick = {() => setActive(false)}>
                <div className={active ? "modal-window active" : "modal-window"}
                onClick = {e => e.stopPropagation()}
                >
                    {children}
                    
                </div>
            </div>
        </div>
    );
}

export default Modal;