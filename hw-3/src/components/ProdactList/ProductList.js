import React, { useState, useEffect  } from 'react';
import ProductItem from '../ProductItem/ProductItem';

function ProductList(){
    
    const [itemArr, setItemArr] = useState({
        isLoaded: false,
        items: []
      })
    
      useEffect(() => {
              fetch("http://localhost:3004/items/")  
            .then(res => res.json())
            .then(
                (res) => {
                    setItemArr({
                        isLoaded: true,
                        items: res,
                    });
                    console.log(res)
                }
            )
      }, []);

    return (
        <div>
            <div className = "product_list">
                {itemArr.items.map(item => (
                    <ProductItem item = {item}/>
                ))}
            </div>
        </div>
    );

}

export default ProductList;