import React from 'react';
import Button from '../Button/Button';
import FavoritesList from '../FavoritesList/FavoritesList';

function ProductItem(props){
    const {item} = props;

    return (
            <div className = "product_item" key={item.articul}>
                <div className = "product_Img" style = {{ backgroundImage: `url(${item.imgUrl})`}}></div>
                <div className = "product_price_and_favIco">
                    <h3>{item.name}</h3>
                    <FavoritesList item = {item}/>
                    <p>{item.price}</p>
                </div>
                <div>
                    <Button item = {item}/>
                    </div>
            </div>
    );
    }


export default ProductItem;