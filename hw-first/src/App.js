import {Component} from "react";
import './App.css';
import Button from './components/Button/Button';

class App extends Component {
  constructor(props){
    super(props);

}

  render() {
    return (
      <div className = "conteiner">      
      <div className = "buttons">
      <Button 
        buttonText = "Open first modal"
        header = {"Hi Nikita!"}
        mainText = {"Hi Nikita!"}
        closeBtn = {false}
        backgroundColor = {{backgroundColor: "neon",}}
        modalBackground = {{backgroundColor: "neon",}}
        actions = {<><button className = "actionsBtn">Hi</button>
                  <button className = "actionsBtn">Bye</button></>}
        />
        

        <Button 
        buttonText = {"Open second modal"}
        header = {"Do you want to delete this file?"}
        mainText = {"Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"}
        closeBtn = {true}
        backgroundColor = {{backgroundColor: "#E74C3C",}}
        modalBackground = {{backgroundColor: "#E74C3C",}}
        actions = {<><button className = "actionsBtn">Ok</button>
                  <button className = "actionsBtn">Close</button></>}
        />
      </div>
      </div>
    );
  }
}

export default App;